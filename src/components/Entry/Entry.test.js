import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, cleanup } from '@testing-library/react';
import Entry from './Entry'

const mockData = {
    id: '123',
    name: 'foo',
    number: "001",
    maxCP: 9233,
    maxHP: 1017,
    image: 'https://via.placeholder.com/1500',
    types: [
        'dasd', 'asdas'
    ]
}

afterEach(cleanup);

it('Renders normally', () => {
    const { asFragment, getByText } = render(<Entry entry={mockData} />)
    expect(asFragment()).toMatchSnapshot()
    expect(getByText('max HP')).toBeTruthy()
})

it('Renders nested with link', () => {
    const { asFragment } = render(<Entry entry={mockData} isNested={true} hasLink={true} />)
    expect(asFragment()).toMatchSnapshot()
})