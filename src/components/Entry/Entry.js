import React from "react";
import { A } from "hookrouter";
import * as EntryEls from "./styles";
import { keys } from "../../constants";
import { camelToLowerCase } from "../../util";

export function DetailRow({ entry, entryKey }) {
  let val = entry[entryKey];
  const label = camelToLowerCase(entryKey);
  return (
    <EntryEls.Detail>
      <span>{label}</span>
      <span>{typeof val === "object" ? val.join(",  ") : val}</span>
    </EntryEls.Detail>
  );
}

export default function Entry({ entry, hasLink = false, isNested = false }) {
  return (
    <EntryEls.Wrapper isNested={isNested}>
      <EntryEls.Name>{entry.name}</EntryEls.Name>
      <EntryEls.EntryRow>
        <EntryEls.ImgWrapper>
          <EntryEls.Img src={entry.image} />
        </EntryEls.ImgWrapper>
        <EntryEls.Details>
          {keys.map(key => (
            <DetailRow entry={entry} key={key} entryKey={key} />
          ))}
          {hasLink && <A href={`/stylindex-sample/pokemon/${entry.id}`}>More Information</A>}
        </EntryEls.Details>
      </EntryEls.EntryRow>
    </EntryEls.Wrapper>
  );
}
