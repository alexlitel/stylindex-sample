import React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import Entry from '../Entry/'
import * as SingleEntryEls from './styles'
import { FIELDS_FRAGMENT } from '../../constants';

export const GET_ITEM = gql`
    query Item($id: String!) {
        pokemon(id: $id) {
            ...PokemonFragment
            evolutions {
                ...PokemonFragment
            }
        }
    }
    ${FIELDS_FRAGMENT}
`

export default function SingleEntry({id}) {
    const { loading, error, data } = useQuery(GET_ITEM, {
        variables: {
            id
        }
    });

    if (loading) return <div>Loading...</div>
    if (error) return <div>Error loading this page. Please refresh and try again</div>

    return (
        <SingleEntryEls.Wrapper>
        <Entry entry={data.pokemon} />
            {
                    data.pokemon.evolutions && 
                    (
                        <SingleEntryEls.Evolutions>
                        <SingleEntryEls.Title>Evolutions</SingleEntryEls.Title>
                        {data.pokemon.evolutions.map(x =>
                        <Entry entry={x} key={x.id} isNested={true} />
                        )}
                        </SingleEntryEls.Evolutions>
                    )
        
            }
        </SingleEntryEls.Wrapper>
    )
}