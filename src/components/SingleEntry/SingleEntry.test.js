import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { MockedProvider } from '@apollo/react-testing';
import { render, cleanup } from '@testing-library/react';
import SingleEntry, { GET_ITEM } from './SingleEntry'
import { createProviderMock, mockAwait } from '../../tests/utils'
import { act } from 'react-dom/test-utils';

const mockData = {
    __typename: 'Pokemon',
    id: 1,
    name: 'foo' + 1,
    number: "00" + 1,
    maxCP: 9233,
    maxHP: 1017,
    image: 'https://via.placeholder.com/1500',
    types: [
        'type 1', 'type 2'
    ]
}

afterEach(cleanup)

it('Renders error', async () => {
    const providerData = createProviderMock(GET_ITEM, null, {
        pokemons: mockData
    }, true)

    const { asFragment, getByText } = render(
        <MockedProvider mocks={providerData} addTypename={true}>
            <SingleEntry />
        </MockedProvider>
    )

    await act(async () => {
        await mockAwait()

    })
    expect(asFragment()).toMatchSnapshot()
    expect(await getByText('Error loading this page. Please refresh and try again')).toBeTruthy()
})

it('Renders loading', async () => {
    const providerData = createProviderMock(GET_ITEM, null, {})

    const { asFragment, getByText } = render(
        <MockedProvider mocks={providerData} addTypename={false}>
            <SingleEntry />
        </MockedProvider>
    )

    expect(asFragment()).toMatchSnapshot()
    expect(await getByText('Loading...')).toBeTruthy()
})

it('Renders normally', async () => {
    const providerData = createProviderMock(GET_ITEM, null, {
        pokemon: {
            ...mockData, evolutions: Array.from(Array(10).keys()).slice(5, 10).map(x => {
                return {...mockData, id: x + 5, name: 'foo' + x, number: '00' + x}
            })
        }
    })

    const { asFragment, getByText } = render(
        <MockedProvider
            mocks={providerData}
            addTypename={false}
        >
            <SingleEntry />
        </MockedProvider>
    )
    await act(async () => {
        await mockAwait()

    })

    expect(asFragment()).toMatchSnapshot()
    expect(await getByText('foo1')).toBeTruthy()
})

