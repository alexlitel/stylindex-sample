

export function createProviderMock(query, variables = null, data = {}, error = false) {
    let mockObj = {
        request: {
            query,
        },
        result: {
            data
        },
    }

    if (variables) {
        mockObj.request.variables = variables
        mockObj.request.data.pokemon.id = variables.id
    }

    if (error) {
        mockObj.error = new Error('Error')
        mockObj.result = {
            errors: [new Error('da')]
        }
    }

    return [mockObj]
}

export function mockAwait() {
    return new Promise(resolve => setTimeout(resolve, 0));
} 
