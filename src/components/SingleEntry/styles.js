import styled from 'styled-components'

export const Wrapper = styled.div`
    position: relative;
`

export const Title = styled.h2`
    text-align: center;
    font-size: 2rem;
    border: 2px dotted;
    max-width: 90%;
    margin: 2rem auto;
    padding: 2rem;
    text-transform: uppercase;
    letter-spacing: 1.5px;
`


export const Evolutions = styled.div`
    position: relative;
`