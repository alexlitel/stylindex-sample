import React from 'react';
import styled, { createGlobalStyle } from 'styled-components'
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from '@apollo/react-hooks'
import { useRoutes, A } from 'hookrouter'
import SingleEntry from './components/SingleEntry/'
import List from './components/List/'

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
      "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
      sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
`

const Container = styled.div`
  margin: 0 auto;
  padding: 0;
  max-width: 1200px;
  color: #444;
  font-family: sans-serif;
`

const Title = styled.h1`
  text-align: center;
  font-weight: 600;
  padding: 10px 0;
  font-size: 2.7rem;
  a {
    color: #444;
    text-decoration: none;
  }
`
const client = new ApolloClient({
  uri: 'https://pokemon-samdavies.stylindex.now.sh/',
});

const routes = {
  '/stylindex-sample/': () => <List />,
  '/stylindex-sample/pokemon/:id': ({id}) => <SingleEntry id={id} />,
}


function App() {
  const routeResult = useRoutes(routes)
  return (
    <ApolloProvider client={client}>
      <GlobalStyle />
      <Container>
      <Title><A href="/" title="Go Home">Pokemon App</A></Title>
      {routeResult || <div>Missing</div> }
      </Container>
    </ApolloProvider>
  );
}

export default App;
