
export const camelToLowerCase = (str) => str.replace(/(?:[a-z])([A-Z])/g, s => s.split('').join(' '))