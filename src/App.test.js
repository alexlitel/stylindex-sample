import React from 'react';
import { useRedirect } from 'hookrouter';
import '@testing-library/jest-dom/extend-expect'
import { render, cleanup } from '@testing-library/react';
import App from './App';

afterEach(cleanup)

it('Home path renders without crashing', () => {
  const Wrapper = () => {
    useRedirect('/', '/stylindex-sample/')
    return <App />
  }
  const { asFragment } = render(<Wrapper />)
  

  expect(asFragment()).toMatchSnapshot()
});

it('Entry path renders without crashing', () => {
  
  const Wrapper = () => {
    useRedirect('/', '/stylindex-sample/pokemon/1')
    return <App />
  }
  const { asFragment } = render(<Wrapper />)
  expect(asFragment()).toMatchSnapshot()
});

it('Error renders without crashing', () => {
  
  const Wrapper = () => {
    useRedirect('/', '/whatever')
    return <App />
  }
  const { asFragment } = render(<Wrapper />)
  expect(asFragment()).toMatchSnapshot()
});
