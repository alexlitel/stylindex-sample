import React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import Entry from '../Entry/'
import { FIELDS_FRAGMENT } from '../../constants';

export const GET_ITEMS = gql`
    query PokemonItems {
        pokemons(first: 10) {
            ...PokemonFragment
        }
    }
    ${FIELDS_FRAGMENT}
`


export default function List() {
    const { loading, error, data } = useQuery(GET_ITEMS);
    if (loading) return <div>Loading...</div>
    if (error) return <div>Error loading this page. Please refresh and try again</div>

    return (
        <div>
            {
                data.pokemons.map(entry =>
                    <Entry entry={entry} key={entry.id} hasLink={true} />
                )
            }
        </div>
    )
}