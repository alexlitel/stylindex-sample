import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { MockedProvider } from '@apollo/react-testing';
import { render, cleanup } from '@testing-library/react';
import List, { GET_ITEMS } from './List'
import { createProviderMock, mockAwait } from '../../tests/utils'
import { act } from 'react-dom/test-utils';

const mockData = Array.from(Array(10).keys()).map(x => ({
    __typename: 'Pokemon',
    id: x,
    name: 'foo' + x,
    number: "00" + x,
    maxCP: 9233,
    maxHP: 1017,
    image: 'https://via.placeholder.com/1500',
    types: [
        'type 1', 'type 2'
    ]
}))

afterEach(cleanup)

it('Renders error', async () => {
    const providerData = createProviderMock(GET_ITEMS, null, {
        pokemons: mockData
    }, true)

    const { asFragment, getByText } = render(
        <MockedProvider mocks={providerData} addTypename={true}>
            <List />
        </MockedProvider>
    )

    await act(async () => {
        await mockAwait()

    })
    expect(asFragment()).toMatchSnapshot()
    expect(await getByText('Error loading this page. Please refresh and try again')).toBeTruthy()
})

it('Renders loading', async () => {
    const providerData = createProviderMock(GET_ITEMS, null, {})

    const { asFragment, getByText } = render(
        <MockedProvider mocks={providerData} addTypename={false}>
            <List />
        </MockedProvider>
    )

    expect(asFragment()).toMatchSnapshot()
    expect(await getByText('Loading...')).toBeTruthy()
})

it('Renders normally', async () => {
    const providerData = createProviderMock(GET_ITEMS, null, {
        pokemons: mockData
    })

    const { asFragment, getByText } = render(
        <MockedProvider 
            mocks={providerData}
            addTypename={false}
            >
            <List />
        </MockedProvider>
    )
    await act(async () => {
        await mockAwait()

    })

    expect(asFragment()).toMatchSnapshot()
    expect(await getByText('foo1')).toBeTruthy()
})

