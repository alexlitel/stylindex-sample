import styled from 'styled-components'

export const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    padding: 0 0 40px;
    position: relative;
    ${props => props.isNested && `
        max-width: 90%;
        margin: auto;
    `}

`

export const EntryRow = styled.div`
display: flex;
width: 100%;
justify-content: center;
flex-direction: column;
    @media(min-width:768px) {
        flex-direction: row;
    }

`

export const Name = styled.div`
font-size: 2rem;
color: white;
background: red;
text-align: center;
padding: 1rem;
margin: 1rem;
font-weight: 600;

`

export const ImgWrapper = styled.div`
max-width: 90%;
margin: auto;
border: 2px solid lightgray;
padding: 15px;
max-height: 270px;
height: 270px;
display: flex;
position: relative;
    @media(min-width:768px) {
        margin: 0 50px 0 0;
        max-width: 390px;
        width: 300px;
    }

    @media(min-width:900px) {
        width: 390px;
    }

`

export const Img = styled.img`
max-width: 80%;
margin: auto;
max-height: 100%;
object-fit: contain;
height: 100%;
`

export const Details = styled.div`
margin: 20px auto;
width: 270px;
max-width: 90%;
    @media(min-width:768px) {
        margin: auto 50px;
    }
`

export const Detail = styled.div`
    padding-bottom: 10px;
    span {
        display: inline-block;
        &:first-child {
            font-weight: bold;
            text-transform: uppercase;
            font-size: 1.25rem;
            margin-right: 10px;
        }
    }
`