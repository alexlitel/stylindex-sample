import gql from "graphql-tag";

export const keys = [
    'id',
    'number',
    'maxCP',
    'maxHP',
    'types',
]

export const FIELDS_FRAGMENT = gql`
    fragment PokemonFragment on Pokemon {
        __typename
        id
        number
        name
        maxCP
        maxHP
        image
        types
    }
`